Express Client
==============

Install
-------

    npm install

Start
-----

    npm start

Start with ` nodemon `
_(Alternatively, you can use nodemon instead of node.)_
-----

    npm run nodemon
> about nodemon: http://nodemon.io/

Test
----

    npm test

Implement
----
See TODO in `routes.js`
